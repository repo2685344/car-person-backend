package com.example.BootDemo.dto;

import com.example.BootDemo.models.Car;
import com.example.BootDemo.models.Owner;



public class CarOwner {

    private Owner owner;
    private Car car;

    public CarOwner(Owner owner, Car car){
        this.owner = owner;
        this.car = car;
    }

    public CarOwner() {
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }
}
