package com.example.BootDemo.controllers;

import com.example.BootDemo.dto.Resp;
import com.example.BootDemo.models.Car;
import com.example.BootDemo.models.CarRepo;
import com.example.BootDemo.models.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@RestController
public class CarController {
    @Autowired
    CarRepo carRepo;

    @GetMapping("/car-search")
    public Resp findAll() {
        Iterable<Car> cars = carRepo.findAll();
        return new Resp(1, cars);
    }

    @GetMapping("/car-search-model/{model}")
    public List<Car> findByModel(@PathVariable String model) {
        return carRepo.findByModel(model);
    }

    @GetMapping("/car-search-price/{price}")
    public List<Car> findByPrice(@PathVariable Integer price) {
        return carRepo.findByMaxPrice(price);
    }

    @GetMapping("/car-search-price-range/{minPrice}/{maxPrice}")
    public List<Car> findByPriceRange(@PathVariable Integer minPrice, @PathVariable Integer maxPrice) {
        return carRepo.findByPriceRange(minPrice, maxPrice);
    }

    @PostMapping("/car-add")
    public Car save(@RequestBody Car newCar) {
        Car c = new Car();
        c.setBrand(newCar.getBrand());
        c.setModel(newCar.getModel());
        c.setColor(newCar.getColor());
        c.setRegisterNumber(newCar.getRegisterNumber());
        c.setPrice(newCar.getPrice());
        c.setYear(newCar.getYear());
        c.setOwner(newCar.getOwner());
        carRepo.save(c);
        return c;
    }

    @DeleteMapping("/car-delete/{id}")

    public String delete(@PathVariable Long id){
        Optional<Car> opt = carRepo.findById(id);
        if (opt.isPresent()){
            Car c = opt.get();
            carRepo.delete(c);
            return "car deleted";
        } else {
            return "car does not exist";
        }
    }
}




