package com.example.BootDemo.models;

import org.springframework.data.repository.CrudRepository;

public interface OwnerRepo extends CrudRepository<Owner,Long> {

}
