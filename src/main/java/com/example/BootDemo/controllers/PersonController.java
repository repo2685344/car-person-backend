package com.example.BootDemo.controllers;

import com.example.BootDemo.models.Person;
import com.example.BootDemo.models.PersonRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Optional;

@RestController
public class PersonController {
    @Autowired
    PersonRepo personRepo; //dependency injection
    @GetMapping("/person-list")
    public Iterable<Person> personList(){
        return personRepo.findAll();
    }

    @PostMapping("/person-save")
    public Person save(@RequestBody Person newPerson){
        Person p = new Person();
        p.setName(newPerson.getName());
        p.setAge(newPerson.getAge());
        p.setSalary(newPerson.getSalary());
        personRepo.save(p);
        return p;
    }

    @PutMapping("/person-update/{id}")
    public Person update(@RequestBody Person newPerson, @PathVariable Long id){
        Optional<Person> opt = personRepo.findById(id);
        if( opt.isPresent()){
            Person p = opt.get();
            p.setName(newPerson.getName());
            p.setAge(newPerson.getAge());
            p.setSalary(newPerson.getSalary());
            personRepo.save(p);
            return p;
        } else {
            return new Person();
        }
    }

    @DeleteMapping("/person-delete/{id}")

    public String delete(@PathVariable Long id){
        Optional<Person> opt = personRepo.findById(id);
        if (opt.isPresent()){
            Person p = opt.get();
            personRepo.delete(p);
            return "user deleted";
        } else {
            return "user does not exist";
        }
    }

    @GetMapping("/person/{id}")
    public Person find(@PathVariable Long id){
        Optional<Person> opt = personRepo.findById(id);
        if(opt.isPresent()){
            return opt.get();
        } else {
            return new Person();
        }
    }
}
