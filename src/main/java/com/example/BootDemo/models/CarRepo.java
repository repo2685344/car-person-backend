package com.example.BootDemo.models;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CarRepo extends CrudRepository<Car, Long> {

    List<Car> findByModel(String model);

    @Query("select c from Car c where c.price <= %?1%")
    List<Car> findByMaxPrice(Integer price);

    @Query("select c from Car c where c.price >= %?1% and c.price <= %?2%")
    List<Car> findByPriceRange(Integer minPrice, Integer maxPrice);
}