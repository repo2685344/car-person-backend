package com.example.BootDemo.controllers;

import com.example.BootDemo.dto.Resp;
import com.example.BootDemo.models.Car;
import com.example.BootDemo.models.Owner;
import com.example.BootDemo.models.OwnerRepo;
import com.example.BootDemo.models.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class OwnerController {
    @Autowired
    OwnerRepo ownerRepo;

    @PostMapping("/owner-add")
    public Owner save(@RequestBody Owner newOwner) {
        Owner o = new Owner();
        o.setName(newOwner.getName()); // Use setName instead of setname
        o.setPhoneNo(newOwner.getPhoneNo()); // Use setPhoneNo instead of setphoneNo

        ownerRepo.save(o);
        return o;
    }

    @GetMapping("/owner-list")
    public Resp findAll() {
        Iterable<Owner> owners = ownerRepo.findAll();
        return new Resp(1, owners);
    }
    @GetMapping("/owner/{id}")
    public Owner findOne(@PathVariable Long id){
        Optional<Owner> opt = ownerRepo.findById(id);
        if (opt.isPresent()){
            return opt.get();
        } else {
            return new Owner();
        }

    }

    @DeleteMapping("/owner-delete/{id}")

    public String delete(@PathVariable Long id){
        Optional<Owner> opt = ownerRepo.findById(id);
        if (opt.isPresent()){
            Owner o = opt.get();
            ownerRepo.delete(o);
            return "owner deleted";
        } else {
            return "owner does not exist";
        }
    }
}
