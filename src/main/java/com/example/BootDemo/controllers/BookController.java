package com.example.BootDemo.controllers;

import com.example.BootDemo.models.Car;
import lombok.Getter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class BookController {

    @GetMapping("/book-list")
    @ResponseBody
    public String bookList(){
        return "Book List";
    }

    @GetMapping("/book-info")
    @ResponseBody
    public String bookInfo(Model model){
        model.addAttribute("name", "Karim");
        return "book_info";
    }

    @GetMapping("/car-list")
    public String carList(Model model){
//        Iterable<Car> itr = carRepo.findAll();
//        model.addAttribute("cars", itr)
        return "car_list";
    }
}
