package com.example.BootDemo.controllers;

import com.example.BootDemo.dto.CarOwner;
import com.example.BootDemo.dto.Resp;
import com.example.BootDemo.models.Car;
import com.example.BootDemo.models.Owner;
import com.example.BootDemo.models.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StaffController {
    @Autowired
    JdbcTemplate jdbcTemplate;
    @GetMapping("/staff-listing")
    public List<String> staffListing(){
        //List<String> myList = new ArrayList<>();
        String sql = "SELECT concat(name, '**' )AS myname FROM person";
        return jdbcTemplate.query(sql,(rs, row) -> {
            return (rs.getString("myname"));
        });
    }

    @GetMapping("/staff-listing2")
    public List<Person> staffListing2(){
        String sql = "SELECT * FROM person";
        return jdbcTemplate.query(sql,(rs, row) -> {
            Person p = new Person();
            p.setName (rs.getString("name"));
            p.setAge(rs.getInt("age"));
            p.setSalary(rs.getInt("salary"));
            return p;
        });
    }

    @GetMapping("/staff-listing3")
    public Resp staffListing3(){
        String sql = "SELECT * FROM car c, OWNER o WHERE c.owner = o.ownerid;";

        List<CarOwner> list =  jdbcTemplate.query(sql, (rs, row) -> {

            Car c = new Car();
            c.setBrand(rs.getString("brand"));

            Owner o = new Owner();
            o.setName(rs.getString("name"));

            CarOwner co = new CarOwner();
            co.setCar(c);
            co.setOwner(o);
            return co;
        });
        return new Resp(1,list);
    }
}
